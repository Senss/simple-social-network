# Simple social network app

## How to start?
Simply run `docker-compose up`.

## What are the endpoints?
Visit http://0.0.0.0:8000/api/schema/swagger-ui/ to discover.
