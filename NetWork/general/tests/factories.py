from factory import Faker, SubFactory, LazyAttribute
from factory.django import DjangoModelFactory
from general.models import (
    User,
    Post,
    Comment,
    Reaction,
    Chat,
    Messenger,
)


class UserFactory(DjangoModelFactory):
    class Meta:
        model = User

    username = Faker("user_name")
    first_name = Faker("first_name")
    last_name = Faker("last_name")
    email = Faker("email")
    is_staff = True


class PostFactory(DjangoModelFactory):
    class Meta:
        model = Post

    author = SubFactory(UserFactory)
    title = Faker("word")
    body = Faker("text")


class CommentFactory(DjangoModelFactory):
    class Meta:
        model = Comment

    body = Faker("text")
    author = SubFactory(UserFactory)
    post = SubFactory(PostFactory)


class ReactionFactory(DjangoModelFactory):
    class Meta:
        model = Reaction

    value = Reaction.Values.SMILE
    author = SubFactory(UserFactory)
    post = SubFactory(PostFactory)


class ChatFactory(DjangoModelFactory):
    class Meta:
        model = Chat

    user_1 = SubFactory(UserFactory)
    user_2 = SubFactory(UserFactory)


class MessageFactory(DjangoModelFactory):
    class Meta:
        model = Messenger

    content = Faker("text")
    author = SubFactory(UserFactory)
    chat = LazyAttribute(lambda obj: ChatFactory(user_1=obj.author))
