from django.contrib import admin
from django.contrib.auth.models import Group
from general.models import (
    Post,
    User,
    Comment,
    Reaction,
)
from datetime import datetime, timezone
from rangefilter.filters import DateRangeFilter
from general.filters import AuthorFilter, PostFilter
from django_admin_listfilter_dropdown.filters import ChoiceDropdownFilter

admin.site.unregister(Group)


@admin.register(User)
class UserModelAdmin(admin.ModelAdmin):
    list_display = (
        "id",
        "first_name",
        "last_name",
        "username",
        "email",
        "is_staff",
        "is_superuser",
        "is_active",
        "date_joined",
        "get_join_day_count"
    )

    fieldsets = (
        (
            "Личные данные", {
                "fields": (
                    "first_name",
                    "last_name",
                    "email",
                )
            }
        ),
        (
            "Учетные данные", {
                "fields": (
                    "username",
                    "password",
                )
            }
        ),
        (
            "Статусы", {
                "classes": (
                    "collapse",
                ),
                "fields": (
                    "is_staff",
                    "is_superuser",
                    "is_active",
                )
            }
        ),
        (
            None, {
                "fields": (
                    "friends",
                )
            }
        ),
        (
            "Даты", {
                "fields": (
                    "date_joined",
                    "last_login",
                    "get_join_day_count",
                )
            }
        )

    )

    search_fields = (
        "id",
        "username",
        "email",
    )

    list_filter = (
        "is_staff",
        "is_superuser",
        "is_active",
        ("date_joined", DateRangeFilter),
    )

    def get_join_day_count(self, obj):
        return (datetime.now(timezone.utc) - obj.date_joined).days

    get_join_day_count.short_description = "days joined"

    readonly_fields = (
        "date_joined",
        "last_login",
        "get_join_day_count",
    )


@admin.register(Post)
class PostModelAdmin(admin.ModelAdmin):
    list_display = (
        "id",
        "author",
        "title",
        "get_body",
        "get_comment_count",
        "created_at",
    )

    search_fields = (
        "id",
        "title",
    )

    list_filter = (
        AuthorFilter,
        ("created_at", DateRangeFilter),
    )

    def get_body(self, obj):
        max_length = 64
        if len(obj.body) > max_length:
            return obj.body[:61] + "..."
        return obj.body

    def get_comment_count(self, obj):
        return obj.comments.count()

    def get_queryset(self, request):
        return super().get_queryset(request).prefetch_related("comments")

    get_body.short_description = "body"
    get_comment_count.short_description = "comments"

    readonly_fields = (
        "created_at",
    )


@admin.register(Comment)
class CommentModelAdmin(admin.ModelAdmin):
    list_display = (
        "id",
        "author",
        "body",
        "post",
        "get_post_title",
        "created_at",
    )

    search_fields = (
        "id",
    )

    list_filter = (
        AuthorFilter,
        PostFilter,
    )

    def get_post_title(self, obj):
        return obj.post.title

    get_post_title.short_description = "Post title"

    readonly_fields = (
        "created_at",
    )


@admin.register(Reaction)
class ReactionModelAdmin(admin.ModelAdmin):
    list_display = (
        "id",
        "author",
        "post",
        "get_post_title",
        "value",
    )

    search_fields = (
        "id",
    )

    list_filter = (
        AuthorFilter,
        PostFilter,
        ("value", ChoiceDropdownFilter),
    )

    def get_post_title(self, obj):
        return obj.post.title

    get_post_title.short_description = "Post title"

    # TODO: fix duplicated code
