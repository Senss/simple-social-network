FROM python:3.10.13


SHELL ["/bin/bash", "-c"]

ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

RUN pip install --upgrade pip

WORKDIR .
COPY requirements.txt .
RUN pip install -r requirements.txt
COPY . .
